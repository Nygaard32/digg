package se.digg.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import se.digg.api.request.KundApi;
import se.digg.pojo.Kund;

class KundRequestTillKundTest {
    Konvertera konvertera = new Konvertera();

    @ParameterizedTest
    @CsvSource(value = {"0701233456;0701233456", "070-1233456;0701233456", "070 1233456;0701233456",
    "+46701233456;46701233456"}, delimiter = ';')
    void gar_skapa_kunder_med_olika_mobilnummer(String mobilnummer, String forvantatMobil) {
        KundApi kundRequest = new KundApi("namn", "adress", "epost@epost.se", mobilnummer);
        Kund forvantadKund = new Kund("namn", forvantatMobil, "epost@epost.se", "adress", true);
        Kund kund = konvertera.kundRequestTillKund(kundRequest);
        assertEquals(forvantadKund, kund);
    }
    
}
