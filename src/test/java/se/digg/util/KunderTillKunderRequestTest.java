package se.digg.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import se.digg.api.request.KundApi;
import se.digg.pojo.Kund;

class KunderTillKunderRequestTest {
    Konvertera konvertera = new Konvertera();
    List<Kund> kunder = new ArrayList<>();

    @Test
    void tom_lista_skapas_da_ingen_rest_skapad_kund_har_gjorts() {
        List<KundApi> apiKunder = konvertera.kunderTillKunderRequest(kunder);
        assertTrue(apiKunder.isEmpty());
    }

    @Test
    void lista_skapas_med_apikunder_da_rest_har_anropats() {
        kunder.add(new Kund("namn", "mobilnummer", "epost", "adress", true));
        List<KundApi> apiKunder = konvertera.kunderTillKunderRequest(kunder);
        assertEquals(apiKunder.size(), 1);
    }

    @Test
    void lista_innehaller_bara_rest_skapade_kunder() {
        kunder.add(new Kund("namn", "mobilnummer", "epost", "adress", true));
        kunder.add(new Kund("namn", "mobilnummer", "epost", "adress", false));
        List<KundApi> apiKunder = konvertera.kunderTillKunderRequest(kunder);
        assertEquals(apiKunder.size(), 1);
    }
}
