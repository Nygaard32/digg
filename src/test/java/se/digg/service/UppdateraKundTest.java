package se.digg.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import se.digg.api.request.KundApi;
import se.digg.api.request.UppdateraApi;
import se.digg.api.response.ResultatApi;
import se.digg.pojo.Kund;
import se.digg.util.Konvertera;

class UppdateraKundTest {
    KundSerivce service;

    @BeforeEach
    void init() {
        service = new KundSerivce(mock(Konvertera.class));
    }
    
    @Test
    void kund_finns_inte() {
        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(new Kund("kola", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true));
        
        ResultatApi response = service.uppdateraKund(new UppdateraApi());
        assertAll(() -> assertEquals(service.kunder.size(), 2),
        () -> assertFalse(response.isLyckades()),
        () -> assertEquals("Kund finns inte", response.getMeddelande()));
    }

    @Test
    void kan_uppdatera_epost() {
        Kund konverteradKund = new Kund("kola", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true);
        Kund existerandeKund = new Kund(konverteradKund);
        Kund forvantadUppdatering = new Kund("kola", "467012345678L", "glass@godis.se", "kolavägen 12", true);
        service.kunder.add(existerandeKund);

        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(konverteradKund);
        
        ResultatApi response = service.uppdateraKund(new UppdateraApi(new KundApi(), null, null, "glass@godis.se", null));
        assertAll(() -> assertEquals(service.kunder.size(), 3),
        () -> assertTrue(response.isLyckades()),
        () -> assertEquals("Lyckades", response.getMeddelande()),
        () -> assertEquals(forvantadUppdatering, service.kunder.get(2)));
    }

    @Test
    void kan_uppdatera_namn() {
        Kund konverteradKund = new Kund("kola", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true);
        Kund existerandeKund = new Kund(konverteradKund);
        Kund forvantadUppdatering = new Kund("glass", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true);
        service.kunder.add(existerandeKund);

        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(konverteradKund);
        
        ResultatApi response = service.uppdateraKund(new UppdateraApi(new KundApi(), "glass", null, null, null));
        assertAll(() -> assertEquals(service.kunder.size(), 3),
        () -> assertTrue(response.isLyckades()),
        () -> assertEquals("Lyckades", response.getMeddelande()),
        () -> assertEquals(forvantadUppdatering, service.kunder.get(2)));
    }

    @Test
    void kan_uppdatera_adress() {
        Kund konverteradKund = new Kund("kola", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true);
        Kund existerandeKund = new Kund(konverteradKund);
        Kund forvantadUppdatering = new Kund("kola", "467012345678L", "kola.glass@godis.se", "mangovägen 12", true);
        service.kunder.add(existerandeKund);

        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(konverteradKund);
        
        ResultatApi response = service.uppdateraKund(new UppdateraApi(new KundApi(), null, "mangovägen 12", null, null));
        assertAll(() -> assertEquals(service.kunder.size(), 3),
        () -> assertTrue(response.isLyckades()),
        () -> assertEquals("Lyckades", response.getMeddelande()),
        () -> assertEquals(forvantadUppdatering, service.kunder.get(2)));
    }

    @Test
    void kan_uppdatera_mobilnummer() {
        Kund konverteradKund = new Kund("kola", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true);
        Kund existerandeKund = new Kund(konverteradKund);
        Kund forvantadUppdatering = new Kund("kola", "123456787", "kola.glass@godis.se", "kolavägen 12", true);
        service.kunder.add(existerandeKund);

        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(konverteradKund);
        
        ResultatApi response = service.uppdateraKund(new UppdateraApi(new KundApi(), null, null, null, "123456787"));
        assertAll(() -> assertEquals(service.kunder.size(), 3),
        () -> assertTrue(response.isLyckades()),
        () -> assertEquals("Lyckades", response.getMeddelande()),
        () -> assertEquals(forvantadUppdatering, service.kunder.get(2)));
    }

    @Test
    void ska_inte_ga_att_uppdatera_kund_sa_att_den_blir_lika_med_existerande_kund() {
        Kund konverteradKund = new Kund("kola", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true);
        Kund existerandeKund = new Kund(konverteradKund);
        service.kunder.add(existerandeKund);
        Kund fjolarsKund = new Kund("Janne", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true);;
        service.kunder.add(fjolarsKund);

        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(konverteradKund);
        
        ResultatApi response = service.uppdateraKund(new UppdateraApi(new KundApi(), null, null, null, null));
        assertAll(() -> assertFalse(response.isLyckades()),
        () -> assertEquals("Kund finns sedan tidigare", response.getMeddelande()));
    }
}
