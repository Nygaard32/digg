package se.digg.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import se.digg.api.request.KundApi;
import se.digg.api.response.ResultatApi;
import se.digg.pojo.Kund;
import se.digg.util.Konvertera;

class LaggTillKundTest {

    KundSerivce service;

    @BeforeEach
    void init() {
        service = new KundSerivce(mock(Konvertera.class));
    }
    
    @Test
    void kan_skapa_ny_kund() {
        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(new Kund("namn", "998799", "epost", "adress", true));

        service.skapaKund(new KundApi());
        assertEquals(service.kunder.size(), 3);
    }

    @Test
    void kan_inte_lagga_till_dubletter() {
        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(new Kund("banan", "467012345678", "kola.banan@godis.se", "kolavägen 12", true));

        ResultatApi response = service.skapaKund(new KundApi());
        assertAll(() -> assertEquals(service.kunder.size(), 2),
        () -> assertFalse(response.isLyckades()),
        () -> assertEquals("Kund finns sedan tidigare", response.getMeddelande()));
    }

    @Test
    void kan_lagga_till_snarlika_kunder_sa_lange_epost_namn_eller_mobilnummer_skiljer() {
        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(new Kund("kola", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true),
        new Kund("kola", "467012345678L", "glass.kola@godis.se", "kolavägen 12", true),
        new Kund("kola", "887012345678L", "kola.glass@godis.se", "kolavägen 12", true),
        new Kund("glass", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true));
        for(int i = 1; i<=4; i++) {
            ResultatApi response = service.skapaKund(new KundApi());
            assertAll(() -> assertTrue(response.isLyckades()),
            () -> assertEquals("Lyckades", response.getMeddelande()));
        }
        assertEquals(service.kunder.size(), 6);
    }
}
