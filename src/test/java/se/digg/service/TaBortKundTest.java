package se.digg.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import se.digg.api.request.KundApi;
import se.digg.api.response.ResultatApi;
import se.digg.pojo.Kund;
import se.digg.util.Konvertera;

class TaBortKundTest {
    KundSerivce service;

    @BeforeEach
    void init() {
        service = new KundSerivce(mock(Konvertera.class));
    }
    
    @Test
    void kan_ta_bort_kund() {
        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(new Kund("banan", "467012345678", "kola.banan@godis.se", "kolavägen 12", true));

        ResultatApi response = service.taBortKund(new KundApi());
        assertAll(() -> assertEquals(service.kunder.size(), 1),
        () -> assertTrue(response.isLyckades()),
        () -> assertEquals("Lyckades", response.getMeddelande()));
    }

    @Test
    void kan_inte_ta_bort_icke_existerande_kund() {
        when(service.konverterare.kundRequestTillKund(any()))
        .thenReturn(new Kund("kola", "467012345678L", "kola.glass@godis.se", "kolavägen 12", true));
        
        ResultatApi response = service.taBortKund(new KundApi());
        assertAll(() -> assertEquals(service.kunder.size(), 2),
        () -> assertFalse(response.isLyckades()),
        () -> assertEquals("Kund finns inte", response.getMeddelande()));
    }
}
