package se.digg.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import io.quarkus.test.junit.QuarkusTest;
import se.digg.api.request.KundApi;

/*Får inte tags att funka så döper om från IT till test :( */
@QuarkusTest
class ITSkapaKundTest {
    @Test
    void gar_att_skapa_kund() {
		KundApi kund = new KundApi("namn", "adress", "namn.adress+2342@eksl.kgm", "0998799");
		String json = skapaJson(kund);
    	given()
    	.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));
    }
    
    @Test
    void skapa_med_tom_kund() {
		KundApi kund = new KundApi();
		String json = skapaJson(kund);
    	given()
		.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(400);
    }

	@ParameterizedTest
	@NullAndEmptySource
	void skapa_utan_namn_gar_ej(String namn) {
		KundApi kund = new KundApi(namn, "adress", "epost@epost.se", "98-984");
		String json = skapaJson(kund);
		given()
		.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(400);
	}

	@ParameterizedTest
	@NullAndEmptySource
	void skapa_utan_adress_gar_ej(String adress) {
		KundApi kund = new KundApi("namn", adress, "epost@epost.se", "98-984");
		String json = skapaJson(kund);
		given()
		.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(400);
	}

	@ParameterizedTest
	@NullAndEmptySource
	@ValueSource(strings = {"epost@epost@mail.com", "epost+4982@.com", "epost@", "epost+343@epost.com."})
	void skapa_utan_epost_eller_med_felaktigt_format_gar_ej(String epost) {
		KundApi kund = new KundApi("namn", "adress", epost, "98-984");
		String json = skapaJson(kund);
		given()
		.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(400);
	}

	@ParameterizedTest
	@NullAndEmptySource
	@ValueSource(strings = {"hjga09889983", "070_1233456", "070&1233456", "+46070&1233456"})
	void skapa_utan_telefonnummer_eller_med_felaktigt_format_gar_ej(String telefonnummer) {
		KundApi kund = new KundApi("namn", "adress", "epost@epost.se", telefonnummer);
		String json = skapaJson(kund);
		given()
		.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(400);
	}

	@ParameterizedTest
	@ValueSource(strings = {" epost@epost.com", "epost@epost.com ", " epost@epost.com "})
	void skapa_med_mellanslag_framfor_eller_efter(String input) {
		KundApi kund = new KundApi(input, input, input, "83484342");
		String json = skapaJson(kund);
		given()
		.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(400);
	}

	String skapaJson(Object object) {
		Jsonb jsonb = JsonbBuilder.create();
		return jsonb.toJson(object);
	}
}