package se.digg.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import se.digg.api.request.KundApi;

/*Får inte tags att funka så döper om från IT till test :( */
@QuarkusTest
class ITTaBortKundTest {
	@Test
    void gar_att_ta_bort_skapad_kund() {
		KundApi kund = new KundApi("nämnö", "ådress", "namn.adress@eksl.kgm", "0998799");
		String json = skapaJson(kund);
    	given()
    	.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));

    	given()
    	.when()
        .body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.delete("/tabort")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));
    }

    @Test
    void kan_inte_ta_bort_oexisterande_kund() {
		KundApi kund = new KundApi("nämno", "aådress", "namn.adress@eksl.kgm", "0998799");
		String json = skapaJson(kund);

    	given()
    	.when()
        .body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.delete("/tabort")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Kund finns inte"),
		"lyckades", is(false));
    }

	String skapaJson(Object object) {
		Jsonb jsonb = JsonbBuilder.create();
		return jsonb.toJson(object);
	}
}
