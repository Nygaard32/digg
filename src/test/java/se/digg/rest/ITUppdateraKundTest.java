package se.digg.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import io.quarkus.test.junit.QuarkusTest;
import se.digg.api.request.KundApi;
import se.digg.api.request.UppdateraApi;

/*Får inte tags att funka så döper om från IT till test :( */
@QuarkusTest
class ITUppdateraKundTest {
    @Test
    void kan_inte_uppdatera_kund_med_null() {
        KundApi kund = new KundApi("namn", "null_adress", "namn.adress@eksl.kgm", "0998799");
		String json = skapaJson(kund);
        skapaKund(json);

        UppdateraApi uppdateraApi = new UppdateraApi(kund, null, null, null, null);
		String uppdaterJson = skapaJson(uppdateraApi);
		given()
    	.body(uppdaterJson)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/uppdatera")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Kund finns sedan tidigare"),
		"lyckades", is(false));
    }

    @ParameterizedTest
    @ValueSource(strings = {" hej", "hej ", " hej "})
    void uppdatera_kund_ska_inte_släppa_igenom_saker_med_blankslag_fore_och_efter_ord(String uppdateratNamn) {
        KundApi kund = new KundApi("namn", "adress", "namn.adress@eksl.kgm", "0998799");
        UppdateraApi uppdateraApi = new UppdateraApi(kund, uppdateratNamn, null, null, null);
		String uppdaterJson = skapaJson(uppdateraApi);
		given()
    	.body(uppdaterJson)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/uppdatera")
    	.then()
    	.statusCode(400);
    }

    private void skapaKund(String json) {
		given()
    	.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));
	}

	String skapaJson(Object object) {
		Jsonb jsonb = JsonbBuilder.create();
		return jsonb.toJson(object);
	}
}
