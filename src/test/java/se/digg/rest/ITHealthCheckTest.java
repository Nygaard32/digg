package se.digg.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.everyItem;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import se.digg.api.request.KundApi;

@QuarkusTest
class ITHealthCheckTest {
    @Test
    void finns_inga_kunder_att_hamta() {
        String kund1 = skapaJson(new KundApi("banan", "kolavägen 12", "kola.banan@godis.se", "467012345678"));
		String kund2 = skapaJson(new KundApi("banan", "kolavägen 12", "banan.kola@godis.se", "467012345678"));
        taBortKund(kund1);
        taBortKund(kund2);

        given()
        .when()
        .get("/health")
        .then()
        .statusCode(200)
        .body("status", is("UP"))
        .body("checks.size()", is(2))
        .body("checks.name", everyItem(anyOf(
                is("Kunder att hämta"),
                is("Går att hämta kunder"))))
        .body("checks.status", everyItem(is("UP")))
        .body("checks[0].data.Kunder", is("Finns inga kunder"))
        .body("checks[1].data.'hamtakunder/alla'", is("Kunder saknas"))
        .body("checks[1].data.hamtakunder", is("Kunder saknas"));
    }

    @Test
    void finns_bara_forskapta_kunder_att_hamta() {
        given()
        .when()
        .get("/health")
        .then()
        .statusCode(200)
        .body("status", is("UP"))
        .body("checks.size()", is(2))
        .body("checks.name", everyItem(anyOf(
                is("Kunder att hämta"),
                is("Går att hämta kunder"))))
        .body("checks.status", everyItem(is("UP")))
        .body("checks[0].data.Kunder", is("Finns kunder"))
        .body("checks[1].data.'hamtakunder/alla'", is("Finns kunder att hämta"))
        .body("checks[1].data.hamtakunder", is("Kunder saknas"));
    }

    @Test
    void finns_kunder_att_hamta() {
        String kund = skapaJson(new KundApi("Häst", "kolavägen 12", "kola.banan@godis.se", "467012345678"));
        skapaKund(kund);

        given()
        .when()
        .get("/health")
        .then()
        .statusCode(200)
        .body("status", is("UP"))
        .body("checks.size()", is(2))
        .body("checks.name", everyItem(anyOf(
                is("Kunder att hämta"),
                is("Går att hämta kunder"))))
        .body("checks.status", everyItem(is("UP")))
        .body("checks[0].data.Kunder", is("Finns kunder"))
        .body("checks[1].data.'hamtakunder/alla'", is("Finns kunder att hämta"))
        .body("checks[1].data.hamtakunder", is("Finns kunder att hämta"));
    }

    private void skapaKund(String json) {
		given()
    	.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));
	}

	private void taBortKund(String json) {
		given()
    	.when()
        .body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.delete("/tabort")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));
	}

    String skapaJson(Object object) {
        Jsonb jsonb = JsonbBuilder.create();
		return jsonb.toJson(object);
	}
}
