package se.digg.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import se.digg.api.request.KundApi;
import se.digg.api.request.UppdateraApi;
import se.digg.api.response.HamtaKunderApi;

/*Får inte tags att funka så döper om från IT till test :( */
@QuarkusTest
class ITHamtaKunderTest {
	@Test
	/**
	 * På grund av exekveringsordning kan jag inte har ett separat test för att
	 * kontrollera att ingen av förskapade kunderna hämtas.
	 */
    void gar_att_na_och_att_hamta_skapad_kund() {
		given()
    	.when()
    	.get("/hamtakunder")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true),
		"kunder.size()", is(0));
		
		KundApi kund = new KundApi("nämnö", "ådress", "namn.adress@eksl.kgm", "0998799");
		String json = skapaJson(kund);
    	skapaKund(json);

    	given()
    	.when()
    	.get("/hamtakunder")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true),
		"kunder.get(0).adress", is("ådress"),
		"kunder.get(0).epost", is("namn.adress@eksl.kgm"),
		"kunder.get(0).mobilnummer", is("0998799"),
		"kunder.get(0).namn", is("nämnö"));

		taBortKund(json);
    }

	@Test
    void hamta_skapad_kund_blir_mindre_vid_borttagning() {
		KundApi kund = new KundApi("Kund1234", "ådress", "namn.adress@eksl.kgm", "0998799");
		String json = skapaJson(kund);
    	
		skapaKund(json);

		HamtaKunderApi kunder = given()
    	.when()
    	.get("/hamtakunder")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true),
		"kunder.size()", is(not(0)))
		.extract().body().as(HamtaKunderApi.class);

		taBortKund(json);

		given()
    	.when()
    	.get("/hamtakunder")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true),
		"kunder.size()", is(kunder.getKunder().size()-1));
    }

	@Test
    void hamta_uppdaterad_kund() {
		KundApi kund = new KundApi("Update", "ådress", "namn.adress@eksl.kgm", "0998799");
		String json = skapaJson(kund);
    	skapaKund(json);

		given()
    	.when()
    	.get("/hamtakunder")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true),
		"kunder.size()", is(not(0)));

		UppdateraApi uppdateraApi = new UppdateraApi(kund, "Uppdaterad", null, null, null);
		String uppdaterJson = skapaJson(uppdateraApi);
		given()
    	.body(uppdaterJson)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/uppdatera")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));

		given()
    	.when()
    	.get("/hamtakunder")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true),
		"kunder.size()", is(not(0)),
		"kunder.get(0).namn", is("Uppdaterad"));

		KundApi uppdateradKund = new KundApi("Uppdaterad", "ådress", "namn.adress@eksl.kgm", "0998799");
		String deleteJson = skapaJson(uppdateradKund);
		taBortKund(deleteJson);
    }

	@Test
	void hamtakunder_alla_ska_innehalla_mer_kunder_an_hamtakunder() {
		HamtaKunderApi hamtadeKunder= given()
    	.when()
    	.get("/hamtakunder")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true))
		.extract().as(HamtaKunderApi.class);

		HamtaKunderApi allaHamtadeKunder = given()
    	.when()
    	.get("/hamtakunder/alla")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true))
		.extract().as(HamtaKunderApi.class);

		assertTrue(allaHamtadeKunder.getKunder().size() > hamtadeKunder.getKunder().size());
	}

	private void skapaKund(String json) {
		given()
    	.body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.when()
    	.post("/skapa")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));
	}

	private void taBortKund(String json) {
		given()
    	.when()
        .body(json)
    	.header("Content-Type", MediaType.APPLICATION_JSON)
    	.delete("/tabort")
    	.then()
    	.statusCode(200)
		.body("meddelande", is("Lyckades"),
		"lyckades", is(true));
	}

	String skapaJson(Object object) {
		Jsonb jsonb = JsonbBuilder.create();
		return jsonb.toJson(object);
	}
}