package se.digg.service;

import static se.digg.api.response.ResponseTexter.LYCKADES;
import static se.digg.api.response.ResponseTexter.KUND_FINNS_INTE;
import static se.digg.api.response.ResponseTexter.KUND_FINNS_SEDAN_TIDIGARE;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;

import se.digg.api.request.KundApi;
import se.digg.api.request.UppdateraApi;
import se.digg.api.response.HamtaKunderApi;
import se.digg.api.response.ResultatApi;
import se.digg.pojo.Kund;
import se.digg.util.Konvertera;

@ApplicationScoped
public class KundSerivce {

	List<Kund> kunder;
	Konvertera konverterare;

	public KundSerivce(Konvertera konverterare) {
		kunder = new ArrayList<>();
		kunder.add(new Kund("banan", "467012345678", "kola.banan@godis.se", "kolavägen 12", false));
		kunder.add(new Kund("banan", "467012345678", "banan.kola@godis.se", "kolavägen 12", false));
		this.konverterare = konverterare;
	}

	public ResultatApi skapaKund(KundApi request) {
		Kund kund = konverterare.kundRequestTillKund(request);
		if (finnsKund(kund)) {
			return new ResultatApi(KUND_FINNS_SEDAN_TIDIGARE, false);
		} else {
			kunder.add(kund);
			return new ResultatApi(LYCKADES, true);
		}

	}

	public ResultatApi taBortKund(KundApi request) {
		Kund kund = konverterare.kundRequestTillKund(request);
		if (kunder.removeIf(existerandeKund -> existerandeKund.equals(kund))) {
			return new ResultatApi(LYCKADES, true);
		} else {
			return new ResultatApi(KUND_FINNS_INTE, false);
		}
	}

	public HamtaKunderApi hamtaRestSkapadeKunder() {
		List<KundApi> responseKunder = konverterare.kunderTillKunderRequest(kunder);
		return new HamtaKunderApi(LYCKADES, true, responseKunder);
	}

	public HamtaKunderApi hamtaAllaKunder() {
		List<KundApi> responseKunder = konverterare.allaKunderTillKunderRequest(kunder);
		return new HamtaKunderApi(LYCKADES, true, responseKunder);
	}

	public ResultatApi uppdateraKund(UppdateraApi anrop) {
		Kund existerandKund = konverterare.kundRequestTillKund(anrop.getExisterandeKund());
		/*
		 * Klonar kund så att man kan göra en kontroll om kunden finns sedan tidigare
		 * med de nya värdena
		 */
		Kund funnenKund = kunder.stream().filter(kund -> kund.equals(existerandKund)).findAny().orElse(null);
		if (Objects.nonNull(funnenKund)) {
			Kund klonadUppdateradKund = new Kund(funnenKund);
			anrop.getUppdateradAdress().ifPresent(klonadUppdateradKund::uppdateraAdress);
			anrop.getUppdateradEpost().ifPresent(klonadUppdateradKund::uppdateraEpost);
			anrop.getUppdateradMobilnummer().ifPresent(klonadUppdateradKund::uppdateraMobil);
			anrop.getUppdateradNamn().ifPresent(klonadUppdateradKund::uppdateraNamn);

			if (finnsKund(klonadUppdateradKund)) {
				return new ResultatApi(KUND_FINNS_SEDAN_TIDIGARE, false);
			} else {
				kunder.add(klonadUppdateradKund);
				kunder.remove(funnenKund);
				return new ResultatApi(LYCKADES, true);
			}
		}
		return new ResultatApi(KUND_FINNS_INTE, false);
	}

	private boolean finnsKund(Kund kund) {
		return kunder.stream().filter(existerandeKund -> existerandeKund.equals(kund)) //
		.findFirst().isPresent();
	}

	public boolean finnsRestSkapaKunder() {
		return kunder.stream().filter(kund -> kund.isRestSkapad()).findAny().isPresent();
	}

	public boolean finnsKunder() {
		return !kunder.isEmpty();
	}

}
