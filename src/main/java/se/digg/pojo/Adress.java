package se.digg.pojo;

public class Adress {
    String gata;
    Integer postnummer;
    String ort;
    String land;
    String landskod;

    public Adress(String gata) {
        this.gata = gata;
    }

    public Adress(Adress adress) {
        this.gata = adress.gata;
    }

    public Adress(String gata, Integer postnummer, String ort, String land, String landskod) {
        this.gata = gata;
        this.postnummer = postnummer;
        this.ort = ort;
        this.land = land;
        this.landskod = landskod;
    }

    public String getGata() {
        return gata;
    }

    public void setGata(String gata) {
        this.gata = gata;
    }

    public Integer getPostnummer() {
        return postnummer;
    }

    public void setPostnummer(Integer postnummer) {
        this.postnummer = postnummer;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public String getLandskod() {
        return landskod;
    }

    public void setLandskod(String landskod) {
        this.landskod = landskod;
    }

    

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gata == null) ? 0 : gata.hashCode());
        result = prime * result + ((land == null) ? 0 : land.hashCode());
        result = prime * result + ((landskod == null) ? 0 : landskod.hashCode());
        result = prime * result + ((ort == null) ? 0 : ort.hashCode());
        result = prime * result + ((postnummer == null) ? 0 : postnummer.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Adress other = (Adress) obj;
        if (gata == null) {
            if (other.gata != null)
                return false;
        } else if (!gata.equals(other.gata))
            return false;
        if (land == null) {
            if (other.land != null)
                return false;
        } else if (!land.equals(other.land))
            return false;
        if (landskod == null) {
            if (other.landskod != null)
                return false;
        } else if (!landskod.equals(other.landskod))
            return false;
        if (ort == null) {
            if (other.ort != null)
                return false;
        } else if (!ort.equals(other.ort))
            return false;
        if (postnummer == null) {
            if (other.postnummer != null)
                return false;
        } else if (!postnummer.equals(other.postnummer))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Adress [gata=" + gata + ", land=" + land + ", landskod=" + landskod + ", ort=" + ort + ", postnummer="
                + postnummer + "]";
    }

}
