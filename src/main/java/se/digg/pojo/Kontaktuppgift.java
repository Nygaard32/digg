package se.digg.pojo;

public class Kontaktuppgift {
    String mobil;
    String fasttelefon;
    String epost;

    public Kontaktuppgift(String mobil, String epost) {
        this.mobil = mobil;
        this.epost = epost;
    }

    public Kontaktuppgift(String mobil, String fasttelefon, String epost) {
        this.mobil = mobil;
        this.fasttelefon = fasttelefon;
        this.epost = epost;
    }

    public Kontaktuppgift(Kontaktuppgift kontaktuppgift) {
        this.mobil = kontaktuppgift.mobil;
        this.fasttelefon = kontaktuppgift.fasttelefon;
        this.epost = kontaktuppgift.epost;
    }

    public String getMobil() {
        return mobil;
    }
    
    public void setMobil(String mobil) {
        this.mobil = mobil;
    }

    public String getFasttelefon() {
        return fasttelefon;
    }

    public void setFasttelefon(String fasttelefon) {
        this.fasttelefon = fasttelefon;
    }

    public String getEpost() {
        return epost;
    }

    public void setEpost(String epost) {
        this.epost = epost;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((epost == null) ? 0 : epost.hashCode());
        result = prime * result + ((fasttelefon == null) ? 0 : fasttelefon.hashCode());
        result = prime * result + ((mobil == null) ? 0 : mobil.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Kontaktuppgift other = (Kontaktuppgift) obj;
        if (epost == null) {
            if (other.epost != null)
                return false;
        } else if (!epost.equals(other.epost))
            return false;
        if (fasttelefon == null) {
            if (other.fasttelefon != null)
                return false;
        } else if (!fasttelefon.equals(other.fasttelefon))
            return false;
        if (mobil == null) {
            if (other.mobil != null)
                return false;
        } else if (!mobil.equals(other.mobil))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Kontaktuppgifter [epost=" + epost + ", fasttelefon=" + fasttelefon + ", mobil=" + mobil + "]";
    }

}
