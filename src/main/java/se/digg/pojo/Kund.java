package se.digg.pojo;

public class Kund {
    String namn;
    Kontaktuppgift kontaktuppgift;
    Adress adress;
    boolean restSkapad;

    public Kund(String namn, String mobilnummer, String epost, String adress, boolean restSkapad) {
        this.namn = namn;
        this.kontaktuppgift = new Kontaktuppgift(mobilnummer, epost);
        this.adress = new Adress(adress);
        this.restSkapad = restSkapad;
    }

    public Kund (Kund kund) {
        this.namn = kund.namn;
        this.kontaktuppgift = new Kontaktuppgift(kund.kontaktuppgift);
        this.adress = new Adress(kund.adress);
        this.restSkapad = kund.restSkapad;
    }

    public String getNamn() {
        return namn;
    }

    public void uppdateraNamn(String namn) {
        this.namn = namn;
    }

    public Kontaktuppgift getKontaktuppgift() {
        return kontaktuppgift;
    }

    public void uppdateraEpost(String epost) {
        this.kontaktuppgift.setEpost(epost);
    }

    public void uppdateraMobil(String mobil) {
        this.kontaktuppgift.setMobil(mobil);
    }

    public Adress getAdress() {
        return adress;
    }

    public void uppdateraAdress(String gata) {
        this.adress.setGata(gata);
    }

    public boolean isRestSkapad() {
        return restSkapad;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adress == null) ? 0 : adress.hashCode());
        result = prime * result + ((kontaktuppgift == null) ? 0 : kontaktuppgift.hashCode());
        result = prime * result + ((namn == null) ? 0 : namn.hashCode());
        result = prime * result + (restSkapad ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Kund other = (Kund) obj;
        if (adress == null) {
            if (other.adress != null)
                return false;
        } else if (!adress.equals(other.adress))
            return false;
        if (kontaktuppgift == null) {
            if (other.kontaktuppgift != null)
                return false;
        } else if (!kontaktuppgift.equals(other.kontaktuppgift))
            return false;
        if (namn == null) {
            if (other.namn != null)
                return false;
        } else if (!namn.equals(other.namn))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Kund [adress=" + adress + ", kontaktuppgift=" + kontaktuppgift + ", namn=" + namn + "]";
    }

}
