package se.digg.rest;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Readiness;

import se.digg.service.KundSerivce;

@Readiness
@ApplicationScoped
public class HamtbaraHealthCheck implements HealthCheck{

    KundSerivce service;

    public HamtbaraHealthCheck(KundSerivce service) {
        this.service = service;
    }

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder status = HealthCheckResponse.named("Går att hämta kunder");
        if(service.finnsKunder()) {
            status.up().withData("hamtakunder/alla", "Finns kunder att hämta");
            if(service.finnsRestSkapaKunder()) {
                status.up().withData("hamtakunder", "Finns kunder att hämta");
            } else {
               status.withData("hamtakunder", "Kunder saknas");
            }
            return status.build();
        } else {
            return status.up().withData("hamtakunder/alla", "Kunder saknas") //
            .withData("hamtakunder", "Kunder saknas").build();
        }
    }
    
    
}
