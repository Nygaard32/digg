package se.digg.rest;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import se.digg.api.request.KundApi;
import se.digg.api.response.ResultatApi;
import se.digg.service.KundSerivce;

@Path("/skapa")
public class SkapaEndpoint {

    KundSerivce service;

    public SkapaEndpoint(KundSerivce service) {
        this.service = service;
    }

    @POST
    public ResultatApi skapaKund(@Valid KundApi anrop)  {
        return service.skapaKund(anrop);
    }
}