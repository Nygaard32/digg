package se.digg.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import se.digg.api.response.HamtaKunderApi;
import se.digg.service.KundSerivce;

@Path("/hamtakunder")
public class HamtaEndpoint {

    KundSerivce service;

    public HamtaEndpoint(KundSerivce service) {
        this.service = service;
    }

    @GET
    public HamtaKunderApi hamtaRestSkapadeKunder() {
        return service.hamtaRestSkapadeKunder();
    }

    @Path("alla")
    @GET
    public HamtaKunderApi hamtaSkapadeKunder() {
        return service.hamtaAllaKunder();
    }
}
