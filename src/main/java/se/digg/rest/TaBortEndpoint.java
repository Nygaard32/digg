package se.digg.rest;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;

import se.digg.api.request.KundApi;
import se.digg.api.response.ResultatApi;
import se.digg.service.KundSerivce;

@Path("/tabort")
public class TaBortEndpoint {

    KundSerivce service;

    public TaBortEndpoint(KundSerivce service) {
        this.service = service;
    }

    @DELETE
    public ResultatApi taBortKund(@Valid KundApi anrop)  {
        return service.taBortKund(anrop);
    }
}