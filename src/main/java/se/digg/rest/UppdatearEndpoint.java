package se.digg.rest;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import se.digg.api.request.UppdateraApi;
import se.digg.api.response.ResultatApi;
import se.digg.service.KundSerivce;

@Path("/uppdatera")
public class UppdatearEndpoint {

    KundSerivce service;

    public UppdatearEndpoint(KundSerivce service) {
        this.service = service;
    }

    @POST
    public ResultatApi skapaKund(@Valid UppdateraApi anrop)  {
        return service.uppdateraKund(anrop);
    }
}