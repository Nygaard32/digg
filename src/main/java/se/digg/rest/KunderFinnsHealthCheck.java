package se.digg.rest;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Liveness;

import se.digg.service.KundSerivce;

@Liveness
@ApplicationScoped
public class KunderFinnsHealthCheck implements HealthCheck{

    KundSerivce service;

    public KunderFinnsHealthCheck(KundSerivce service) {
        this.service = service;
    }

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder status = HealthCheckResponse.named("Kunder att hämta");
        if(service.finnsKunder()) {
            return status.up().withData("Kunder", "Finns kunder").build();
        } else {
            return status.up().withData("Kunder", "Finns inga kunder").build();
        }
    }
    
    
}
