package se.digg.api.response;

public class ResultatApi {
	private String meddelande;
	private boolean lyckades;
	
	public ResultatApi() {
	}
	
	public ResultatApi(String meddelande, boolean lyckades) {
		this.meddelande = meddelande;
		this.lyckades = lyckades;
	}

	public ResultatApi(ResponseTexter meddelande, boolean lyckades) {
		this.meddelande = meddelande.getText();
		this.lyckades = lyckades;
	}
	
	/**
	 * Getter för att skriva ut json response 
	 * (Behövs för jsonb tolkning av object)
	 * @return meddelande text
	 */
	public String getMeddelande() {
		return meddelande;
	}

	/**
	 * Getter för att skriva ut json response
	 * @return lyckat anrop
	 */
	public boolean isLyckades() {
		return lyckades;
	}

	@Override
	public String toString() {
		return "Resultat [lyckades=" + lyckades + ", meddelande=" + meddelande + "]";
	}
}
