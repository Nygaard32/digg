package se.digg.api.response;

public enum ResponseTexter {
    LYCKADES("Lyckades"),
    KUND_FINNS_INTE("Kund finns inte"),
    KUND_FINNS_SEDAN_TIDIGARE("Kund finns sedan tidigare");

    private final String responseText;
    ResponseTexter(String responseText) {
        this.responseText = responseText;
    }

    public String getText(){
        return this.responseText;
    }
    
}
