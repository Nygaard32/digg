package se.digg.api.response;

import java.util.Collections;
import java.util.List;

import se.digg.api.request.KundApi;

public class HamtaKunderApi extends ResultatApi {
    List<KundApi> kunder;

    public HamtaKunderApi() {
    }

    public HamtaKunderApi(List<KundApi> kunder) {
        this.kunder = kunder;
    }

    public HamtaKunderApi(String meddelande, boolean lyckades, List<KundApi> kunder) {
        super(meddelande, lyckades);
        this.kunder = Collections.unmodifiableList(kunder);
    }

    public HamtaKunderApi(ResponseTexter meddelande, boolean lyckades, List<KundApi> kunder) {
        super(meddelande, lyckades);
        this.kunder = Collections.unmodifiableList(kunder);
    }

    public List<KundApi> getKunder() {
        return kunder;
    }

    public void setKunder(List<KundApi> kunder) {
        this.kunder = kunder;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((kunder == null) ? 0 : kunder.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HamtaKunderApi other = (HamtaKunderApi) obj;
        if (kunder == null) {
            if (other.kunder != null)
                return false;
        } else if (!kunder.equals(other.kunder))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ResultatKunder [kunder=" + kunder + "]";
    }
    
}
