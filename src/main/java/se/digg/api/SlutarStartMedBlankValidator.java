package se.digg.api;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SlutarStartMedBlankValidator implements ConstraintValidator<SlutarStartMedBlank, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(Objects.isNull(value)) {
            return true;
        }
        final Pattern pattern = Pattern.compile("^\\s+|\\s+$", Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(value);
        return !matcher.find();
    }

}
