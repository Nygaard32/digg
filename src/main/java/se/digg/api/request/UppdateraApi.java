package se.digg.api.request;

import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;

import se.digg.api.SlutarStartMedBlank;

public class UppdateraApi {
    @NotNull
    @Valid
    KundApi existerandeKund;
    @SlutarStartMedBlank
    String uppdateradNamn;
    @SlutarStartMedBlank
    String uppdateradAdress;
    @Email(message = "ej stöd e-post format")
    String uppdateradEpost;
    @Pattern(flags = Flag.CASE_INSENSITIVE, message = "ej stöd mobilnummer", regexp = "^[0-9-+ ]+$")
    @SlutarStartMedBlank
    String uppdateradMobilnummer;

    public UppdateraApi() {
    }

    public UppdateraApi(KundApi existerandeKund, String uppdateradNamn, String uppdateradAdress, String uppdateradEpost,
            String uppdateradMobilnummer) {
        this.existerandeKund = existerandeKund;
        this.uppdateradNamn = uppdateradNamn;
        this.uppdateradAdress = uppdateradAdress;
        this.uppdateradEpost = uppdateradEpost;
        this.uppdateradMobilnummer = uppdateradMobilnummer;
    }

    public UppdateraApi(KundApi existerandeKund) {
        this.existerandeKund = existerandeKund;
    }

    public KundApi getExisterandeKund() {
        return existerandeKund;
    }

    public void setExisterandeKund(KundApi existerandeKund) {
        this.existerandeKund = existerandeKund;
    }

    public Optional<String> getUppdateradNamn() {
        return Optional.ofNullable(uppdateradNamn);
    }

    public void setUppdateradNamn(String uppdateradNamn) {
        this.uppdateradNamn = uppdateradNamn;
    }

    public Optional<String> getUppdateradAdress() {
        return Optional.ofNullable(uppdateradAdress);
    }

    public void setUppdateradAdress(String uppdateradAdress) {
        this.uppdateradAdress = uppdateradAdress;
    }

    public Optional<String> getUppdateradEpost() {
        return Optional.ofNullable(uppdateradEpost);
    }

    public void setUppdateradEpost(String uppdateradEpost) {
        this.uppdateradEpost = uppdateradEpost;
    }

    public Optional<String> getUppdateradMobilnummer() {
        return Optional.ofNullable(uppdateradMobilnummer);
    }

    public void setUppdateradMobilnummer(String uppdateradMobilnummer) {
        this.uppdateradMobilnummer = uppdateradMobilnummer;
    }
    

}
