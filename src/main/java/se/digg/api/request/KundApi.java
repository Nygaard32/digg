package se.digg.api.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;

import se.digg.api.SlutarStartMedBlank;
import se.digg.pojo.Adress;
import se.digg.pojo.Kontaktuppgift;

public class KundApi {
	
	@NotBlank(message = "namn saknas")
	@SlutarStartMedBlank
	private String namn;
	@NotBlank(message = "adress saknas")
	@SlutarStartMedBlank
	private String adress;
	@NotBlank(message = "e-post saknas")
	@Email(message = "ej stöd e-post format")
	private String epost;
	@NotBlank(message = "mobilnummer saknas")
	@Pattern(flags = Flag.CASE_INSENSITIVE, message = "ej stöd mobilnummer", regexp = "^[0-9-+ ]+$")
	@SlutarStartMedBlank
	private String mobilnummer;

	public KundApi() {
	}

	/**
	 * 
	 * @param namn
	 * @param adress
	 * @param epost
	 * @param mobilnummer
	 */
	public KundApi(String namn, String adress, String epost, String mobilnummer) {
		this.namn = namn;
		this.adress = adress;
		this.epost = epost;
		this.mobilnummer = mobilnummer;
	}

	/**
	 * @param namn
	 * 		{@code String}
	 * @param adress
	 * 		{@code Adress}
	 * @param kontaktuppgift
	 * 		{@code Kontaktuppgift}
	 */
	public KundApi(String namn, Adress adress, Kontaktuppgift kontaktuppgift) {
		this.namn = namn;
		this.adress = adress.getGata();
		this.epost = kontaktuppgift.getEpost();
		this.mobilnummer =	kontaktuppgift.getMobil();
	}

	public String getNamn() {
		return namn;
	}

	public void setNamn(String namn) {
		this.namn = namn;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getEpost() {
		return epost;
	}

	public void setEpost(String epost) {
		this.epost = epost;
	}

	public String getMobilnummer() {
		return mobilnummer;
	}

	public void setMobilnummer(String mobilnummer) {
		this.mobilnummer = mobilnummer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adress == null) ? 0 : adress.hashCode());
		result = prime * result + ((epost == null) ? 0 : epost.hashCode());
		result = prime * result + ((mobilnummer == null) ? 0 : mobilnummer.hashCode());
		result = prime * result + ((namn == null) ? 0 : namn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KundApi other = (KundApi) obj;
		if (adress == null) {
			if (other.adress != null)
				return false;
		} else if (!adress.equals(other.adress))
			return false;
		if (epost == null) {
			if (other.epost != null)
				return false;
		} else if (!epost.equals(other.epost))
			return false;
		if (mobilnummer == null) {
			if (other.mobilnummer != null)
				return false;
		} else if (!mobilnummer.equals(other.mobilnummer))
			return false;
		if (namn == null) {
			if (other.namn != null)
				return false;
		} else if (!namn.equals(other.namn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Skapa [adress=" + adress + ", epost=" + epost + ", namn=" + namn + ", mobilnummer=" + mobilnummer
				+ "]";
	}
}
