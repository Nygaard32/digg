package se.digg.util;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import se.digg.api.request.KundApi;
import se.digg.pojo.Kund;

@ApplicationScoped
public class Konvertera {

    public Kund kundRequestTillKund(KundApi kund) {
        String mobilnummer = formateraMobilnummer(kund.getMobilnummer());
        return new Kund(kund.getNamn(), mobilnummer, kund.getEpost(), kund.getAdress(), true);
    }

    /**
     * Formateringen av mobilnummer till en long, tar bort +, mellanslag och - teckenen
     * @param mobilnummer
     * @return formaterat mobilnummer
     */
    private String formateraMobilnummer(String mobilnummer) {
        return mobilnummer.replaceAll("\\+|\\-|\\s", "");
    }

    public List<KundApi> kunderTillKunderRequest(List<Kund> kunder) {
        List<KundApi> kundRequests = new ArrayList<>();
        for (Kund kund : kunder) {
            if(kund.isRestSkapad()){
                kundRequests.add(new KundApi(kund.getNamn(), kund.getAdress(), kund.getKontaktuppgift()));
            }
        }
        return kundRequests;
    }

    public List<KundApi> allaKunderTillKunderRequest(List<Kund> kunder) {
        List<KundApi> kundRequests = new ArrayList<>();
        for (Kund kund : kunder) {
            kundRequests.add(new KundApi(kund.getNamn(), kund.getAdress(), kund.getKontaktuppgift()));
        }
        return kundRequests;
    }
}
