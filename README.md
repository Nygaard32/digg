# Start av applikation
./mvnw compile quarkus
# Kund arbetsprov för digg 
Detta projekt använder Quarkus.
Jag tror att tjänsten/servern är en microservice, men jag tror också att jag har en ganska skev bild av microservice arkitekturen då jag förställer mig mycket mindre.Nästan som en funktions baserad arkitektur där det går att stänga av varje tjänst för sig själv utan att de påverkar större delen av system.
Alltså mycket mindre än detta, det skulle vara ungefär en quarkus server per rest endpoint stället för att hålla det i quarkus server per ”område”.
# Tidsåtgång
Jag har använt mycket mer tid än 4 timmar på denna server, då jag hade problem med att få ut response objekt på grund av att jag inte förstod att JsonB vill ha getters för att skapa upp response body, satt va ganska stressad över detta problem och tillbringade minst 4 timmar på det felet... Jobbigt och lärorikt (tror det har gått åt 6 till 7 timmar faktiskt).
En pryl jag spenderat lite för mycket tid med är struktur, då jag tycker det är viktigt för att få en bra överblick och namn på klasserna/objekten... oh gud namn som jag inte riktigt nöjd med än. 

Lördagen gick det mycket tid åt att bara få allt att fungera med de extra kontrollerna som jag fick för mig behövdes 

* Se om en kund fanns eller inte sedan tidigare som löstes med en generare equals metod där jag tog bort boolean flaggan restSkapad.
* Egenskapad validator... En validator som jag har skapat på CSN, men fick problem med nu i quarkus av någon anledning
* Försök att uppgradera JsonB beroendet då den nuvarande versionen innehåller säkerhethål enligt Synk analys motor 🙁

Söndagen va mindre eventfull. 

* Fixade så att man inte kunde uppdatera så samma kund fanns i interna minnes listan 2 ggr eller fler.
* Åtgärdade alla IT tester då ja ställde rejält till det med lite refaktorering i uppdatera tjänsten (glömde lägga addera den uppdaterade kunden)
* Refactor så att systemet är lite mer functional programmerings baserad (för kuls skull, ville testa, gjorde lite misstag när ja gjorde refactor 😅)
* La till en applications.properties efter addering av OpenApi
    * OpenApi är precis det jag har sökt för att förstå hur man ska ha hitta vad för resttjänsten en server har \^o^/
* La till enkel health check som visar att de inte finns någon databas
    * Health checken förstår jag inte riktigt anledningen till att ha i denna applikation då den inte går att nå när servern är nere.
    * Okej hittade en sätt att faktiskt använda det på något vettigare sätt, men tjänsterna är aldrig nere (kom på det under promenaden). Nu visas den bara om det finns kunder som går att hämta.
# Problem som har upptäckts
Ett litet problem jag upptäckte är att den kan ibland spara ner åäö fel som jag tror beror på att jag inte har konfigurarat curl anropena korrekt så att de alltid skickar iväg med UTF-8 då felet uppstår via git bash (utvecklar på windows), medan mvn test gör det korrekt mot de endpoints tester som finns.
# Verktyg

* All utveckling har skett i VS code och allt.
* Git bash för curl anrop i slutet när rest anrop via power shell blev jobbigt att förberreda
* Power shell för exekvering av server i dev läge